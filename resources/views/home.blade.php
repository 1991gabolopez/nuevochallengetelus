@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Voting System</div>
                @if(Auth::check())
                    @if(Auth::user()->id_rol == 1)
                        <div class="panel-body" align="center">
                            <a href="{{url('/chooseCommittee')}}" class="btn btn-primary btn-lg btn-plus add-task">Give a vote</a>
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>
@endsection