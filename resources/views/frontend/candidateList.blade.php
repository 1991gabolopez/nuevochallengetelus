@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    @if(Session::has('message'))

                        <div class="alert alert-{{Session::get('tipo')}} alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{Session::get('message')}}
                        </div>
                    @endif
                    <div class="panel-body">
                        <!-- Table-to-load-the-data Part -->
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody id="contry-list" name="contry-list">
                            @foreach ($candidates as $candidate)
                                <tr id="contry{{$candidate->id}}">
                                    <td>{{$candidate->name}}</td>
                                    <td>
                                        <a class="btn btn-primary btn-md" href="{{url('/setVote/'.$candidate->id)}}">Vote</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
