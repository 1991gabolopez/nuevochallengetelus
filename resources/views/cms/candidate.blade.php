@extends('layouts.app')
@section('content')
    <script>
        $(document).ready(function () {



        });
        function sendRequest  (id_departamento) {
           // console.log('token ' + $('input[name=_token]').val());
            $.post("{!! route('getDepartments') !!}", {id_country: id_departamento,_token:$('input[name=_token]').val()}, function(result,status){
                if(status == "success"){
                 console.log('data ' + JSON.stringify(result))
          //  $('#DepartmentSelect').append("<select id='departments' class='form-control'>  </select>");
                }
                else {
                    alert('Error al obtener los departamentos');


                }
            });
        }

    </script>

 <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        @if(Session::has('message'))

                            <div class="alert alert-{{Session::get('tipo')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('message')}}
                            </div>
                        @endif
                        @if(!isset($candidate))
                        {!!Form::open(['route'=>'Candidates.store', 'method'=>'POST'])!!}
                        @else
                            {!!Form::model($candidate, ['route'=>['Candidates.update', $candidate->id], 'method'=>'PUT'])!!}

                        @endif

                            @include('cms.FormCandidate.CandidateForm')
                            {!!Form::submit('Register candidate', ['class'=>'btn btn-primary btn-flat','id'=>'clickable'])!!}

                        {!!  Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection