<div class="form-group">
    {!!  Form::label('Name') !!}
    {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter your name','required']) }}
</div>

<div class="form-group">
    {!!  Form::label('Documents List')  !!}
    {!! Form::select('id_document',$document,null,['class'=>'form-control','required']) !!}
</div>

<div class="form-group">
    {!! Form::label('Document Number') !!}
    {!! Form::text('documentNumber',null,['class'=>'form-control','placeholder'=>'Enter your document number','required']) !!}
</div>
<div class="form-group">
    {!!  Form::label('Countries List')  !!}
    <select class="form-control" id="country" onchange="sendRequest(this.value)" name="id_contry">
        @foreach ($country as $contrie)
            @if(!isset($candidate))
            <option value="{{$contrie->id}}">{{$contrie->contryName}}</option>
            @else
                @if($candidate->idCountry == $contrie->id)
                    <option selected value="{{$contrie->id}}">{{$contrie->contryName}}</option>

                @endif
            @endif
        @endforeach
    </select>
</div>
<div class="form-group">

    <div id="DepartmentSelect">
    @if(isset($candidate))
        {!!  Form::label('Departmen List')  !!}
        {!! Form::select('id_department',$department,null,['class'=>'form-control','required', 'id'=>'department']) !!}

     @endif

    </div>
</div>


<div class="form-group">
    <div id="DepartmentSelect">
            {!!  Form::label('Committee')  !!}
            {!! Form::select('id_committee',$committe,null,['class'=>'form-control','required', 'id'=>'department']) !!}
    </div>
</div>

