@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Department Register</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/departments/setDepartment') }}">
                            {{ csrf_field() }}


                            <div class="form-group{{ $errors->has('departmentName') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Department name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="departmentName">

                                    @if ($errors->has('departmentName'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('departmentName') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('departmentName') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Pais</label>

                                <div class="col-md-6">

                                    <select class="form-control" name="id_contry">
                                        @foreach ($contries as $contrie)
                                        <option value="{{$contrie->id}}">{{$contrie->contryName}}</option>
                                        @endforeach
                                    </select>


                                @if ($errors->has('documentName'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('departmentName') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i> Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection