<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class ListCandidatesController extends Controller
{
    public function getCandiates(Request $request){
        $id = $request->id_committee;
        $candidates = DB::table('candidates')
            ->join('departments', 'candidates.id_department', '=', 'departments.id')
            ->join('contries','departments.id_contry','=','contries.id' )
            ->where('candidates.id_committee',$id)
            ->select('contries.contryName','candidates.*')->get();
        return view('frontend.candidateList', compact('candidates'));
    }
}
