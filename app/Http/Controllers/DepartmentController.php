<?php

namespace App\Http\Controllers;

use App\Contrie;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\Http\Requests;

class DepartmentController extends Controller
{
    public function getDepartment(){
        //$departments = Department::all();

        $departments = DB::table('departments')
            ->join('contries', 'departments.id_contry', '=', 'contries.id')
       ->select('contries.contryName','departments.*')->get();
        return view('cms.department', compact('departments'));
    }

    public function getDepartmentRegister(){
        $contries = Contrie::all();
        return view('cms.departmentRegister', compact('contries'));
    }

    public function setDepartment(){
        $data = request()->all();
        Department::create($data);

        Session::flash('tipo','success');
        Session::flash('message', 'Departamento creado exitosamente');
        return redirect()->to('/departments');
    }

    public function destroy($id){




        Department::destroy($id);
        Session::flash('tipo','success');
        Session::flash('message', 'Departamento eliminado exitosamente');
        return redirect()->to('/departments');
    }
}
