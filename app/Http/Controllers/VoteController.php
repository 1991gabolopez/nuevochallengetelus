<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Candidate;

class VoteController extends Controller
{
    public function setVote($idCandi){

        $objetoCandidato = DB::table('candidates')
            ->select('candidates.*')
            ->where('id', $idCandi)
            ->get();

        $idCommittee = $objetoCandidato[0]->id_committee;

        $votesRecieved = $objetoCandidato[0]->votesRecieved;

        if($votesRecieved==null){
            $votesRecieved= 1;
            #insert

        }else{
            $votesRecieved= $votesRecieved+1;
        }
        return $idCandi;
        $CandidatoParaUpdate= Candidate::find($idCandi);
        $CandidatoParaUpdate->fill(
            [
                'votesRecieved' =>$votesRecieved
            ]
        );

        $CandidatoParaUpdate->save();

    }
}
