<?php

namespace App\Http\Controllers;

use App\Contrie;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Document;
use Illuminate\Support\Facades\DB;
use Session;
class DocumentController extends Controller
{
    public function getDocument(){
        $documents = Document::all();

        return view('cms.document', compact('documents'));
    }

    public function getDocumentRegister(){
        $documents = Contrie::all();

        return view('cms.document', compact('documents'));
        return view('cms.documentRegister');
    }

    public function setDocument(){

        $data = request()->only('documentName');
        Document::create($data);
        return redirect()->to('/documents');
    }

    public function destroy($id){

        $NdocumentosUsuario= DB::table('users')
            ->select(DB::raw('count(*) as N'))
            ->where('id_document', $id)
            ->get();

        $NdocumentosCandidato = DB::table('candidates')
            ->select(DB::raw('count(*) as N'))
            ->where('id_document', $id)
            ->get();



        if($NdocumentosCandidato[0]->N ==0){
            if($NdocumentosUsuario[0]->N == 0){


                Session::flash('tipo','success');
                Session::flash('message', 'Documento Eliminado exitosamente');
                Document::destroy($id);
            }
            else{
                Session::flash('tipo','danger');
                Session::flash('message', 'Documento no pudo ser Eliminado');
            }
        } else{
            Session::flash('tipo','danger');
            Session::flash('message', 'Documento no pudo ser Eliminado');
        }
        $documents = Document::all();


        return view('cms.document', compact('documents'));
    }
}
