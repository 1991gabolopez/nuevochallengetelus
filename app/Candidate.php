<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable = ['id_committee', 'id_department', 'id_document', 'documentNumber',
                            'name', 'votesReceived' ];
                       
}
